# Overview

This module generates an image designed to be used for sharing on social media.
This is done using a custom field type and configurable layout entities.

## Configuration

### Setup
1. Configure the [Cloudinary module](https://drupal.org/project/cloudinary) with
   the required API keys and configure image rules in Cloudinary.
1. Create a Social Image Layout: `/admin/structure/social_image_layout`
1. Add a _Social Image_ field type to a node type.
1. Configure the source image field that will supply the background image.
1. Select the Social Image Layout entity that the current field will use.
1. Re-save the desired nodes to trigger image generation.

### Metatag Module Integration
1. Install and enable the [Metatag](https://www.drupal.org/project/metatag)
   module. Additionally, enable the included `metatag_open_graph` sub-module.
1. Configure the desired node types and set the `og:image` metatag to use the
   image provided by the previously created Social Image field using the
   provided tokens.
