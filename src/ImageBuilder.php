<?php

namespace Drupal\social_media_image_generator;

use Cloudinary\Error;
use Cloudinary\Uploader;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\node\NodeInterface;

/**
 * Provides Social Media Image generation features with cloudinary.com.
 */
class ImageBuilder {

  /**
   * The folder where images are saved.
   */
  const SOCIAL_IMAGE_PATH = 'public://social-image/';

  use StringTranslationTrait;

  /**
   * The logging service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The file usage service.
   *
   * @var \Drupal\file\FileUsage\FileUsageInterface
   */
  protected $fileUsage;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The token system service.
   *
   * @var Drupal\Core\Utility\Token
   */
  protected $tokenSystem;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    LoggerChannelFactoryInterface $logger,
    MessengerInterface $messenger,
    TranslationInterface $string_translation,
    FileUsageInterface $file_usage,
    FileSystemInterface $file_system,
    EntityTypeManagerInterface $entity_type_manager,
    Token $token_system
  ) {
    $this->logger = $logger->get('chromatic_social_image');
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
    $this->fileUsage = $file_usage;
    $this->fileSystem = $file_system;
    $this->entityTypeManager = $entity_type_manager;
    $this->tokenSystem = $token_system;
  }

  /**
   * Generate Social Image and attach to the node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to create social images for.
   *
   * @return bool|null
   *   TRUE if the image was generated successfully, otherwise FALSE.
   */
  public function generateImage(
    NodeInterface $node
  ) {
    // Loop through the social image fields.
    foreach ($this->getSocialImageFields($node) as $social_image_field => $label) {
      $source_image_field = $node->get($social_image_field)->getSourceImageField();

      // $this->logger->notice($social_image_field);
      // $this->logger->notice();
      // Determine if all of the required fields exist, have values, and if the
      // source data field values have changed.
      if (!$this->regenerateImage(
        $node,
        $source_image_field,
        $social_image_field)
      ) {
        return;
      }
      // Load the social image layout entity.
      /** @var \Drupal\social_media_image_generator\Entity\SocialImageLayoutConfigEntity $layout_entity */
      $layout_entity = $this->entityTypeManager
        ->getStorage('social_image_layout')
        ->load($node->get($social_image_field)->getLayoutEntityId());
      if (!$layout_entity) {
        $this->messenger->addError($this->t('Failed to load the %layout layout.', [
          '%layout' => $node->get($social_image_field)->getLayoutEntityId(),
        ]));
        return FALSE;
      }

      // Load the image style to use when sending the image to Cloudinary.
      $image_style = $layout_entity->getImageStyle();
      $image_style_entity = $this->entityTypeManager
        ->getStorage('image_style')
        ->load($image_style);
      if (!$image_style_entity) {
        $this->messenger->addError($this->t('Failed to load the %image_style image style.', [
          '%image_style' => $image_style,
        ]));
        return FALSE;
      }
      // Get the source image file entity.
      $source_file_entity = $this->getReferencedFileEntity($node, $source_image_field);
      // Generate a source image URL using an image style.
      $source_file_url = $image_style_entity->buildUrl($source_file_entity->getFileUri());
      if (!$source_file_url) {
        $this->logger->notice($this->t('Social Image not generated, missing hero image node.'));
        $this->messenger->addStatus($this->t('Social Image not generated, missing hero image node.'));
        return FALSE;
      }
      try {
        $data = ['node' => $node];
        // Prepare title text.
        $text_layer = \Cloudinary::process_layer(
          [
            'font_family' => 'fonts:atramentregularitalic-irfwdg.otf',
            'font_size' => $layout_entity->get('title_font_size'),
            'text' => strtoupper($this->tokenSystem->replace(
              $layout_entity->get('title_text'),
              $data
            )),
          ],
          'overlay'
        );
        // Generate social image.
        $social_cloud = Uploader::upload($source_file_url, [
          'folder' => 'social_media/',
          'overwrite' => TRUE,
          'transformation' => [
            [
              'background' => $layout_entity->get('background_color'),
              'opacity' => $layout_entity->get('background_opacity'),
            ],
            [
              'height' => $layout_entity->get('height'),
              'format' => 'jpg',
              'quality' => $layout_entity->get('quality'),
              'width' => $layout_entity->get('width'),
              'gravity' => 'faces:auto',
              'crop' => 'fill',
            ],
            [
              'overlay' => 'chromatic-logo-alt-pb2chr',
              'height' => $layout_entity->get('logo_height'),
              'x' => $layout_entity->get('logo_x'),
              'y' => $layout_entity->get('logo_y'),
              'crop' => 'scale',
              'gravity' => 'south_east',
            ],
            [
              'color' => $layout_entity->get('title_color'),
              'overlay' => urldecode($text_layer),
              'width' => $layout_entity->get('title_width'),
              'height' => $layout_entity->get('title_height'),
              'x' => $layout_entity->get('title_x'),
              'y' => $layout_entity->get('title_y'),
              'crop' => 'fit',
              'text_align' => 'left',
              'gravity' => 'west',
            ],
            [
              'color' => $layout_entity->get('social_color'),
              'overlay' => [
                'font_family' => 'fonts:metaserifprobook-jusdzi.otf',
                'font_size' => $layout_entity->get('social_font_size'),
                'text' => $this->tokenSystem->replace(
                  $layout_entity->get('social_text'),
                  $data
                ),
              ],
              'x' => $layout_entity->get('social_x'),
              'y' => $layout_entity->get('social_y'),
              'crop' => 'fit',
              'gravity' => 'south_west',
            ],
          ],
        ]);

        // Save the image to Drupal if it was built successfully.
        if (!$social_cloud) {
          $this->messenger->addError($this->t('An error occurred, the social media image was not generated.'));
          return FALSE;
        }
        // Download the file.
        $social_file_data = file_get_contents($social_cloud['url']);
        // Build the file name.
        $social_file_name = self::SOCIAL_IMAGE_PATH . $node->id() . $source_file_entity->getFilename();
        // Prepare the directory.
        $directory = self::SOCIAL_IMAGE_PATH;
        $this->fileSystem->prepareDirectory(
          $directory,
          FileSystemInterface::CREATE_DIRECTORY
        );
        // Save the file.
        $social_file = file_save_data(
          $social_file_data,
          $social_file_name,
          FileSystemInterface::EXISTS_REPLACE
        );
        // Verify that the file saved successfully.
        if (!$social_file) {
          $this->messenger->addError($this->t('Failed to save social image from %url to %file.', [
            '%url' => $social_cloud['url'],
            '%file' => $social_file_name,
          ]));
          return FALSE;
        }

        // Remove existing the social image if one exists.
        if ($file = $node->get($social_image_field)->entity) {
          $this->fileUsage->delete($file, 'social_image_builder');
        }
        // Set new Social Image.
        $node->get($social_image_field)->setValue([
          'target_id' => $social_file->id(),
        ]);
        $this->messenger->addMessage($this->t('Social Image has been generated.'));
        return TRUE;
      }
      catch (Exception $e) {
        // Exception handling if something gets thrown.
        $this->logger->error($e->getMessage());
        $this->messenger->addError($this->t('An error occurred, the social media image was not generated.'));
      }
      catch (Error $e) {
        $this->logger->warning($e->getMessage());
        $this->messenger->addWarning($this->t('Cloudinary Error: :error', [
          ':error' => $e->getMessage(),
        ]));
        $this->messenger->addWarning($this->t('Social media image not generated. Image creation will not work from a local environment.'));
      }
    }
  }

  /**
   * Determines if a node has a social image field.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to check.
   *
   * @return bool
   *   Boolean indicating if there are any social image fields.
   */
  public function hasSocialImageFields(NodeInterface $node): bool {
    return empty($this->getSocialImageFields($node)) ? FALSE : TRUE;
  }

  /**
   * Determines if a node has a social image field.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to check.
   *
   * @return string[]
   *   An array of social image fields keyed by machine name.
   */
  public function getSocialImageFields(NodeInterface $node): array {
    $fields = [];
    foreach ($node->getFieldDefinitions() as $field_definition) {
      if ($field_definition->getType() == 'social_image_file') {
        $fields[$field_definition->getName()] = $field_definition->getLabel();
      }
    }
    return $fields;
  }

  /**
   * Finds the referenced file entity.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The source node.
   * @param string $source_image_field
   *   The source image field.
   *
   * @return \Drupal\file\Entity\File|false
   *   The file entity or FALSE if the field is empty.
   */
  protected function getReferencedFileEntity(NodeInterface $node, string $source_image_field) {
    // Get the file entity from an image field.
    if ($node->get($source_image_field)->getFieldDefinition()->getType() == 'image') {
      return $node->get($source_image_field)->entity;
    }
    // Get the file entity from a media reference field.
    // @todo How do we know what field to use on the media entity? We could loop
    //   through the fields and find the image field.
    return $node->get($source_image_field)->entity->field_media_image->entity;
  }

  /**
   * Check if Social Image needs to be generated.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node to check.
   * @param string $source_image_field
   *   The source image field name.
   * @param string $social_image_field
   *   The social image field name.
   *
   * @return bool
   *   Boolean indicating if the image should be generated.
   */
  protected function regenerateImage(NodeInterface $node, string $source_image_field, string $social_image_field) {
    // Verify that the source field exists.
    if (!$node->hasField($source_image_field)) {
      $message = $this->t('Unable to generate social media image due to missing %source_image_field field.', [
        '%source_image_field' => $source_image_field,
      ]);
      $this->messenger->addWarning($message);
      $this->logger->warning($message);
      return FALSE;
    }
    // Verify that we have the social media image field.
    if (!$node->hasField($social_image_field)) {
      $message = $this->t('Unable to generate social media image due to missing %social_image_field field.', [
        '%social_image_field' => $social_image_field,
      ]);
      $this->messenger->addWarning($message);
      $this->logger->warning($message);
      return FALSE;
    }
    // Skip nodes without a source image.
    if ($node->get($source_image_field)->isEmpty()) {
      $message = $this->t('Unable to generate social media image due to missing %source_image_field image.', [
        '%source_image_field' => $source_image_field,
      ]);
      $this->messenger->addStatus($message);
      $this->logger->warning($message);
      return FALSE;
    }
    // Verify that the referenced media entity has an image.
    // @todo How do we know what field to use on the media entity? We could loop
    //   through the fields and find the image field.
    $field_definition = $node->get($source_image_field)->getFieldDefinition();
    if ($field_definition->getType() == 'entity_reference'
      && $field_definition->getFieldStorageDefinition()->getSetting('target_type') == 'media'
      && $node->get($source_image_field)->entity->field_media_image->isEmpty()
    ) {
      $message = $this->t('Unable to generate social media image due to missing %source_image_field image.', [
        '%source_image_field' => $source_image_field,
      ]);
      $this->messenger->addStatus($message);
      $this->logger->warning($message);
      return FALSE;
    }
    // Always generate images for new nodes.
    if ($node->isNew()) {
      return TRUE;
    }
    // Generate an image if the social media image is missing.
    if ($node->get($social_image_field)->isEmpty()) {
      return TRUE;
    }
    // Regenerate the image when the title changes.
    if (!$node->get('title')->equals($node->original->get('title'))) {
      return TRUE;
    }
    // Regenerate the image when the hero image changes.
    if (!$node->get($source_image_field)->equals($node->original->get($source_image_field))) {
      return TRUE;
    }
    // Default to not generating an image if none of the conditions were met.
    $this->messenger->addStatus($this->t('No social media image generated.'));
    return FALSE;
  }

}
