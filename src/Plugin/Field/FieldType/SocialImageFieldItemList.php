<?php

namespace Drupal\social_media_image_generator\Plugin\Field\FieldType;

use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;

/**
 * Field listing functionality for social image fields.
 */
class SocialImageFieldItemList extends FileFieldItemList {

  /**
   * Get the layout entity ID.
   *
   * @return int
   *   The layout entity ID.
   */
  public function getLayoutEntityId() {
    return $this->getSetting('social_image_layout_id');
  }

  /**
   * Get the source image field name.
   *
   * @return string
   *   The source field machine name.
   */
  public function getSourceImageField() {
    return $this->getSetting('source_field');
  }

}
