<?php

namespace Drupal\social_media_image_generator\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Plugin\Field\FieldType\ImageItem;

/**
 * Plugin implementation of the 'file' field type.
 *
 * @FieldType(
 *   id = "social_image_file",
 *   label = @Translation("Social Image"),
 *   description = @Translation("This field stores the social image."),
 *   category = @Translation("Reference"),
 *   default_widget = "file_generic",
 *   default_layoutter = "file_default",
 *   list_class = "\Drupal\social_media_image_generator\Plugin\Field\FieldType\SocialImageFieldItemList",
 *   constraints = {"ReferenceAccess" = {}, "FileValidation" = {}}
 * )
 */
class SocialImageItem extends ImageItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'source_field' => '',
      'social_image_layout_id' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);

    // Build a list of image fields on the entity type.
    $fields = $this->getSourceImageFields();
    // Prepare description text.
    $description = $this->t('Select an image field to use as the source image.');
    if (empty($fields)) {
      $description = $this->t('No image fields available.');
    }
    // Source field form element.
    $element['source_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Source image field'),
      '#default_value' => $this->getSetting('source_field'),
      '#description' => $description,
      '#disabled' => empty($fields) ? TRUE : FALSE,
      '#options' => $fields,
    ];

    // Build a list of social image layout entities.
    $social_image_layouts = $this->getSocialImageLayoutOptions();
    // Prepare description text.
    $description = $this->t('Select a social image layout.');
    if (empty($social_image_layouts)) {
      $description = $this->t('No social image layouts available.');
    }
    $element['social_image_layout_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Layout'),
      '#default_value' => $this->getSetting('social_image_layout_id'),
      '#description' => $description,
      '#disabled' => empty($social_image_layouts) ? TRUE : FALSE,
      '#options' => $social_image_layouts,
    ];

    return $element;
  }

  /**
   * Get an array of social image layout options.
   *
   * @return string[]
   *   An array of social image layouts keyed by entity ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getSocialImageLayoutOptions(): array {
    $social_image_layouts = [];
    $social_image_layout_entities = \Drupal::entityTypeManager()
      ->getStorage('social_image_layout')
      ->loadMultiple();
    foreach ($social_image_layout_entities as $social_image_layout_entity) {
      $social_image_layouts[$social_image_layout_entity->id()] = $social_image_layout_entity->label();
    }
    return $social_image_layouts;
  }

  /**
   * Get an array of potential source image fields.
   *
   * @return sring[]
   *   An array of field labels keyed by field machine name.
   */
  private function getSourceImageFields(): array {
    $fields = [];
    foreach ($this->getEntity()->getFieldDefinitions() as $field_definition) {
      // Allow image fields.
      if ($field_definition->getType() == 'image') {
        $fields[$field_definition->getName()] = $field_definition->getLabel();
      }
      // Allow media entity reference fields.
      if ($field_definition->getType() == 'entity_reference'
        && $field_definition->getFieldStorageDefinition()->getSetting('target_type') == 'media'
      ) {
        $fields[$field_definition->getName()] = $field_definition->getLabel();
      }
    }
    return $fields;
  }

}
