<?php

namespace Drupal\social_media_image_generator\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Remote Site entity.
 *
 * @ConfigEntityType(
 *   id = "social_image_layout",
 *   label = @Translation("Social Image Layout"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\social_media_image_generator\SocialImageLayoutListBuilder",
 *     "form" = {
 *       "add" = "Drupal\social_media_image_generator\Form\SocialImageLayoutForm",
 *       "edit" = "Drupal\social_media_image_generator\Form\SocialImageLayoutForm",
 *       "delete" = "Drupal\social_media_image_generator\Form\SocialImageLayoutDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\social_media_image_generator\SocialImageLayoutHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "social_image_layout",
 *   admin_permission = "administer social image layout configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/social_image_layout/{social_image_layout}",
 *     "add-form" = "/admin/structure/social_image_layout/add",
 *     "edit-form" = "/admin/structure/social_image_layout/{social_image_layout}/edit",
 *     "delete-form" = "/admin/structure/social_image_layout/{social_image_layout}/delete",
 *     "collection" = "/admin/structure/social_image_layout"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "image_style",
 *     "width",
 *     "height",
 *     "quality",
 *     "background_color",
 *     "background_opacity",
 *     "title_color",
 *     "title_font_size",
 *     "title_height",
 *     "title_width",
 *     "title_text",
 *     "title_x",
 *     "title_y",
 *     "logo_file_id",
 *     "logo_height",
 *     "logo_x",
 *     "logo_y",
 *     "social_color",
 *     "social_font_size",
 *     "social_text",
 *     "social_x",
 *     "social_y",
 *   }
 * )
 */
class SocialImageLayoutConfigEntity extends ConfigEntityBase implements SocialImageLayoutConfigEntityInterface {

  /**
   * The layout ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The layout label.
   *
   * @var string
   */
  protected $label;

  /**
   * The layout image style.
   *
   * @var string
   */
  protected $image_style;

  /**
   * Image width.
   *
   * @var int
   */
  protected $width = 1200;

  /**
   * Image height.
   *
   * @var int
   */
  protected $height = 630;

  /**
   * Image quality (%).
   *
   * @var int
   */
  protected $quality;

  /**
   * Background Color.
   *
   * @var string
   */
  protected $background_color;

  /**
   * Background opacity.
   *
   * @var int
   */
  protected $background_opacity;

  /**
   * Title text.
   *
   * @var string
   */
  protected $title_text;

  /**
   * Title color.
   *
   * @var string
   */
  protected $title_color;

  /**
   * Title font size.
   *
   * @var int
   */
  protected $title_font_size = 96;

  /**
   * Social Image Text Width.
   *
   * @var int
   */
  protected $title_width = 1000;

  /**
   * Social Image Text Height.
   *
   * @var int
   */
  protected $title_height = 350;

  /**
   * Social Image Text X Position.
   *
   * @var int
   */
  protected $title_x = 100;

  /**
   * Social Image Text Y Position.
   *
   * @var int
   */
  protected $title_y = -25;

  /**
   * Logo Height.
   *
   * @var int
   */
  protected $logo_height = 70;

  /**
   * Logo X Position.
   *
   * @var int
   */
  protected $logo_x = 90;

  /**
   * Logo Y Position.
   *
   * @var int
   */
  protected $logo_y = 30;

  /**
   * Logo file ID.
   *
   * @var int
   */
  protected $logo_file_id;

  /**
   * Social text.
   *
   * @var string
   */
  protected $social_text;

  /**
   * Social Color.
   *
   * @var string
   */
  protected $social_color;

  /**
   * Social Font Size.
   *
   * @var int
   */
  protected $social_font_size = 30;

  /**
   * Social X Position.
   *
   * @var int
   */
  protected $social_x = 105;

  /**
   * Social Y Position.
   *
   * @var int
   */
  protected $social_y = 50;

  /**
   * Get the image style.
   *
   * @return string
   *   The image style.
   */
  public function getImageStyle() {
    return $this->image_style;
  }

}
