<?php

namespace Drupal\social_media_image_generator\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining social image layout entities.
 */
interface SocialImageLayoutConfigEntityInterface extends ConfigEntityInterface {

}
