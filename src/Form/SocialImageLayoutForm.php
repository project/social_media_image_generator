<?php

namespace Drupal\social_media_image_generator\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Social Image Layout Config Entity Form.
 */
class SocialImageLayoutForm extends EntityForm {

  /**
   * Default text color.
   */
  const DEFAULT_TEXT_COLOR = '#ffffff';

  /**
   * Build the form.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Layout Name'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'source' => ['label'],
        'exists' => [$this, 'exists'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    // Source Image.
    $form['source'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Source'),
    ];
    // Image style.
    $form['source']['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Style'),
      '#default_value' => $this->entity->getImageStyle(),
      '#description' => $this->t('The image style used to generate the source that is sent to Cloudinary.'),
      '#options' => $this->getImageStyleOptions(),
      '#required' => TRUE,
    ];

    // Generated Image.
    $form['generated'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Generated Image'),
    ];
    // Width.
    $form['generated']['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $this->entity->get('width'),
      '#required' => TRUE,
    ];
    // Height.
    $form['generated']['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->entity->get('height'),
      '#required' => TRUE,
    ];
    // Quality.
    $form['generated']['quality'] = [
      '#type' => 'number',
      '#title' => $this->t('Quality'),
      '#default_value' => $this->entity->get('quality') ?: 100,
      '#field_suffix' => '%',
      '#size' => 3,
      '#min' => 0,
      '#max' => 100,
      '#required' => TRUE,
    ];

    // Background.
    $form['background'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Generated Image'),
    ];
    // Background color.
    $form['background']['background_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Background Color'),
      '#size' => 6,
      '#default_value' => $this->entity->get('background_color'),
      '#required' => TRUE,
    ];
    // Background opacity.
    $form['background']['background_opacity'] = [
      '#type' => 'number',
      '#title' => $this->t('Background Opacity'),
      '#default_value' => $this->entity->get('background_opacity') ?: 30,
      '#size' => 3,
      '#min' => 0,
      '#max' => 100,
      '#field_suffix' => '%',
      '#required' => TRUE,
    ];

    // Token browser.
    $form['tokens'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['node'],
      '#global_types' => TRUE,
      '#show_nested' => FALSE,
    ];

    // Title.
    $form['title'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Title'),
    ];
    // Title text.
    $form['title']['title_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text'),
      '#default_value' => $this->entity->get('title_text'),
      '#required' => TRUE,
    ];
    // Title color.
    $form['title']['title_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Color'),
      '#size' => 6,
      '#default_value' => $this->entity->get('title_color') ?: self::DEFAULT_TEXT_COLOR,
      '#required' => TRUE,
    ];

    // Social.
    $form['social'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Social'),
    ];
    // Social text.
    $form['social']['social_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text'),
      '#description' => $this->t('The social network account name to include with the image.'),
      '#default_value' => $this->entity->get('social_text'),
      '#required' => TRUE,
    ];
    // Social color.
    $form['social']['social_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Color'),
      '#size' => 6,
      '#default_value' => $this->entity->get('social_color') ?: self::DEFAULT_TEXT_COLOR,
      '#required' => TRUE,
    ];

    // Logo.
    $form['logo_file_id'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Logo'),
      '#upload_location' => 'public://social-image-logo/',
      '#default_value' => $this->entity->get('logo_file_id') ?? NULL,
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg svg'],
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $layout_entity = $this->getEntity();
    $file = $this->entityTypeManager
      ->getStorage('file')
      ->load($form_state->getValue(['logo_file_id', '0']));
    if ($file) {
      $file->setPermanent();
      $file->save();
    }
    $status = $layout_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Social Image Layout.', [
          '%label' => $this->entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Social Image Layout.', [
          '%label' => $this->entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

  /**
   * Determines if the remote site entity already exists.
   *
   * @param string $id
   *   The remote site configuration ID.
   *
   * @return bool
   *   TRUE if the remote site configuration exists, FALSE otherwise.
   */
  public function exists($id) {
    $query = $this->entityTypeManager->getStorage('social_image_layout')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $query;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    if ($this->plugin instanceof PluginFormInterface) {
      $this->plugin->validateConfigurationForm($form, $form_state);
    }
  }

  /**
   * Get image style options.
   *
   * @return string[]
   *   An array of image style names keyed by their ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getImageStyleOptions(): array {
    $image_styles = [];
    $image_style_entities = $this->entityTypeManager
      ->getStorage('image_style')
      ->loadMultiple();
    foreach ($image_style_entities as $image_style) {
      $image_styles[$image_style->id()] = $image_style->label();
    }
    return $image_styles;
  }

}
